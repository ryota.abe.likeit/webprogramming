﻿CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

use usermanagement;
CREATE TABLE user(
id  SERIAL PRIMARY KEY UNIQUE AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
create_date DATETIME  NOT NULL,
update_date DATETIME  NOT NULL
);
use usermanagement;
insert into user(login_id,name,birth_date,password,create_date,update_date) 
values('admin','管理者','2000/1/1','aaa',now(),now());
 insert into user(login_id,name,birth_date,password,create_date,update_date) 
values('aaa','あ','2000/1/1','bbb',now()  ,now()  ); 
 insert into user(login_id,name,birth_date,password,create_date,update_date) 
values('aaaa','い','2000/1/1','ccc',now() ,now() ); 
 insert into user(login_id,name,birth_date,password,create_date,update_date) 
values('aaaaa','う','2000/1/1','ddd',now(),now()  ); 
insert into user(login_id,name,birth_date,password,create_date,update_date) 
values('bbb','う','2000/1/1','ddd',now(),now()  ); 

