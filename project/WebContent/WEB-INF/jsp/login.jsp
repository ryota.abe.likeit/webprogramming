<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ログイン画面</title>
    <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"           integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/login.css" rel="stylesheet">
</head>
<body>
     <div class="aa">

    <h1>ログイン画面</h1>

    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

 <form action="LoginServlet" method="post">
    <p>ログインID</p><input type="text" name = "loginId" style="width:200px;" class="form-control">

    <p>パスワード</p><input type="password" name = "password" style="width:200px;" class="form-control">

         <input type="submit" value="ログイン" class="btn btn-primary">
        </form>
  </div>
</body>
</html>