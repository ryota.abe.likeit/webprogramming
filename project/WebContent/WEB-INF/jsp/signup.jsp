<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/signup.css" rel="stylesheet">
</head>
<body>

	<div class="logout">
		<div class="row col-6 aaa">

			<h3>${sessionScope.userInfo.name}さん</h3>
			<a href="LogoutServlet"><h2 class="ml-5">ログアウト</h2></a>
		</div>
		<c:if test="${errMsg !=null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
	</div>
	<form action="UserSignupServlet" method="post">
		<div class="signup">
			<h1>ユーザー新規登録</h1>
			<p>ログインID</p>
			<input type="text" name="loginId" style="width: 500px;"
				class="form-control">

			<p>パスワード</p>
			<input type="password" name="passwordA" style="width: 500px;"
				class="form-control">

			<p>パスワード(確認)</p>
			<input type="password" name="passwordB" style="width: 500px;"
				class="form-control">

			<p>ユーザー名</p>
			<input type="text" name="name" style="width: 500px;"
				class="form-control">

			<p>生年月日</p>
			<input type="text" name="birth" style="width: 500px;"
				class="form-control">
		</div>
		<p></p>

		<div class="botton">
			<input type="submit" value="登録" class="btn btn-primary">
		</div>
	</form>
	<div class="return">
		<a href="UserListServlet">戻る</a>
	</div>
</body>
</html>