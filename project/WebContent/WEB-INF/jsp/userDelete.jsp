<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ削除確認</title>
    <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"               integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   <link href="css/userDelete.css" rel="stylesheet">

</head>
<body>
  <div class="logout">
        <div class="row col-6 aaa">
            <h3>${sessionScope.userInfo.name}さん</h3>
    <a href="LogoutServlet" ><h2 class="ml-5">ログアウト</h2></a>
        </div>
    </div>
    <h1>ユーザ削除確認</h1>
    
    <p class="ml-5">ログインID: ${userB.loginId}</p>
    <p class="ml-5">を本当に削除してよろしいでしょうか。</p>
    <p> </p>
    
     <div class="botton">
        <a  href = "UserListServlet" class="btn btn-primary btn-lg" >キャンセル</a>
		<form action="UserDeleteServlet" method="post">
		<input name ="id" value="${userB.id}"type="hidden">
		<button type="submit" class="btn btn-secondary btn-lg">OK</button>
		</form>
    </div>
</body>
</html>

