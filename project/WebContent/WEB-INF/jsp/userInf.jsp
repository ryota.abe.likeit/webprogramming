<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザー情報詳細参照</title>
    <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"               integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/userInf.css" rel="stylesheet">
</head>
<body>
    <div class="logout">
        <div class="row col-6 aaa">

            <h3>${sessionScope.userInfo.name}さん</h3>
    <a href="LogoutServlet" ><h2 class="ml-5">ログアウト</h2></a>
        </div>
    </div>
   <form action="UserInfServlet" method = "post">
    <div class="userInf">

         <h1>ユーザー情報詳細参照</h1>
        <div class="row col-6 a">
    <p>ログインID</p>
    <p class="ml-3">${userD.loginId}</p>
   </div>

    <div class="row col-6 a">
    <p>ユーザー名</p>
        <p class="ml-3">${userD.name}</p>
         </div>

          <div class="row col-6 a">
    <p> 生年月日</p>
         <p class="ml-3">${userD.birthDate}</p>
         </div>

        <div class="row col-6 a">
    <p>登録日時</p>
            <p class="ml-3">${userD.createDate}</p>
            </div>

         <div class="row col-6 a">
    <p> 更新日時</p>
        <p class="ml-3">${userD.updateDate}</p>
           </div>

        </div>
        </form>
     <div class="return">
      <a href="UserListServlet" >戻る</a>
    </div>

</body>
</html>

