<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/userList.css" rel="stylesheet">
</head>
<body>
	<div class="logout">
		<div class="row col-6 aaa">

			<h3>${sessionScope.userInfo.name }さん</h3>
			<a href="LogoutServlet"><h2 class="ml-5">ログアウト</h2></a>
		</div>
	</div>

	<h1>ユーザ一覧</h1>
	<div class="s">
		<a href="UserSignupServlet"><h4>新規登録</h4></a>
	</div>
	<form action="UserListServlet" method="post">
	<div class="n">
		<P>
			<label>ログインID :<input type="text" name="loginId"></label>
		</P>
		<p>
			<label>ユーザー名 :<input type="text"name="name"></label>
		</p>
		<p>
			<label>生年月日 :<input type="date"name="dateA"></label> ～ <input type="date"name="dateB">
		</p>

	</div>
	<div class="botton">
		<input type="submit" value="検索" class="btn btn-primary">
	</div>
	</form>
	<div class="table">
		<table border="1">
			<tr>
			<tr>
				<th>ログインID</th>
				<th>ユーザ名</th>
				<th>生年月日</th>
				<th></th>
			</tr>

			</tr>
			<c:forEach var="user" items="${userList}">
				<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
					<td><a class="btn btn-primary"
						href="UserInfServrlet?id=${user.id}")>詳細</a> <c:if
							test="${user.loginId == userInfo.loginId ||userInfo.loginId == 'admin'}">
							<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}")>更新</a>
						</c:if> <c:if test="${userInfo.loginId == 'admin'}">

							<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}")>削除</a>
						</c:if></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>