<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/userUpdate.css" rel="stylesheet">
</head>
<body>
	<div class="logout">
		<div class="row col-6 aaa">

			<h3>${sessionScope.userInfo.name}さん</h3>
			<a href="LogoutServlet"><h2 class="ml-5">ログアウト</h2></a>
		</div>
	</div>
	<h1>ユーザー情報更新</h1>
	<c:if test="${errMsg !=null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<form action="UserUpdateServlet" method="post">
		<div class="update">
			<input name="id" value="${userB.id}" type="hidden">
			<p>ログインID</p>
			<input name="loginId" value="${userB.loginId}" type="hidden">
			<p>${userB.loginId}</p>
			<p>パスワード</p>
			<input type="password" name="passwordA" style="width: 500px;"
				class="form-control">

			<p>パスワード(確認)</p>
			<input type="password" name="passwordB" style="width: 500px;"
				class="form-control">

			<p>ユーザー名</p>
			<input value="${userB.name}" type="text" name="name"
				style="width: 500px;" class="form-control">

			<p>生年月日</p>
			<input value="${userB.birthDate}" type="date" name="birth"
				style="width: 500px;" class="form-control">
		</div>
		<p></p>

		<div class="botton">
			<input type="submit" value="登録" class="btn btn-primary">
		</div>
	</form>
	<div class="return">
		<a href="UserListServlet">戻る</a>
	</div>

</body>
</html>


