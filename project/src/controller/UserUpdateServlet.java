package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		 String id = request.getParameter("id");
		 UserDao userdao = new UserDao();
		 User user = userdao.findById(id);
		 request.setAttribute("userB", user);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String loginId = request.getParameter("loginId");
		String passwordA = request.getParameter("passwordA");
		String passwordB = request.getParameter("passwordB");
		String name = request.getParameter("name");
		String birth = request.getParameter("birth");

		UserDao userdao = new UserDao();
		User user = userdao.findById(id);
		
		
		if (name.equals("") || birth.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("userB",user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if (!passwordA.equals(passwordB)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("userB",user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(passwordA.equals("") && passwordB.equals("")){
			userdao.pupdate(name,birth,id);
			response.sendRedirect("UserListServlet");
			return;
		}else {
		userdao.Update(passwordA, name, birth,id);
		response.sendRedirect("UserListServlet");
		return;
		}
	}

}
