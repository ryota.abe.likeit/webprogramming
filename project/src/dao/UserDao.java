package dao;


import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;


public class UserDao {
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id NOT IN('1')";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void signup(String loginId, String passwordA, String name, String birth) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO user(login_Id,password,name,birth_date,create_date,update_date) VALUE(?,?,?,?,NOW(),NOW())";

			String ps = this.password(passwordA);
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, loginId);
			pstmt.setString(2, ps);
			pstmt.setString(3, name);
			pstmt.setString(4, birth);
			int rs = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public User findById(String id) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int idP = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			Date birthData = rs.getDate("birth_date");
			String password = rs.getString("password");
			String signupData = rs.getString("create_date");
			String updateData = rs.getString("update_date");

			return new User(idP, loginIdData, nameData, birthData, password, signupData, updateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void Update(String passwordA, String name, String birth, String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "update user set password =?, name = ?,birth_date=?, update_date=NOW() where id  = ?;";

			PreparedStatement pstmt = conn.prepareStatement(sql);
			String ps = this.password(passwordA);
			pstmt.setString(1, ps);
			pstmt.setString(2, name);
			pstmt.setString(3, birth);
			pstmt.setString(4, id);
			int rs = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void delete(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "delete from  user  where id  = ?;";

			PreparedStatement pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, id);
			int rs = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public User findByLoginId(String loginId) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int idP = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			Date birthData = rs.getDate("birth_date");
			String password = rs.getString("password");
			String signupData = rs.getString("create_date");
			String updateData = rs.getString("update_date");

			return new User(idP, loginIdData, nameData, birthData, password, signupData, updateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void pupdate(String name, String birth, String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "update user set name = ?,birth_date=?, update_date=NOW() where id  = ?;";

			PreparedStatement pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, name);
			pstmt.setString(2, birth);
			pstmt.setString(3, id);
			int rs = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<User> findByUser(String loginId,String name,String dateA,String dateB) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql ="SELECT * FROM user WHERE id NOT IN('1')";
			if(!loginId.equals("")) {
				sql += " and login_id='" + loginId+"'";
			}
			if(!name.equals("")) {
				sql += "and name like'%"+ name +"%'";
			}
			if(!dateA.equals("")) {
				sql += "and birth_date >='"+dateA+"'";
			}
			if(!dateB.equals("")) {
				sql += "and  birth_date <'"+dateB +"'";
			}
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginid = rs.getString("login_id");
				String Name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginid, Name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public String password(String passwordA) {
		String source = passwordA;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		return result;
	}
	}







